package com.example.celina.bguide;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.kontakt.sdk.android.ble.connection.OnServiceReadyListener;
import com.kontakt.sdk.android.ble.manager.ProximityManager;
import com.kontakt.sdk.android.ble.manager.ProximityManagerContract;
import com.kontakt.sdk.android.ble.manager.listeners.IBeaconListener;
import com.kontakt.sdk.android.ble.manager.listeners.simple.SimpleIBeaconListener;
import com.kontakt.sdk.android.common.KontaktSDK;
import com.kontakt.sdk.android.common.profile.IBeaconDevice;
import com.kontakt.sdk.android.common.profile.IBeaconRegion;


public class MainActivity extends AppCompatActivity {

    private ProximityManagerContract proximityManager;
    ProgressBar searchProgressBar;
    TextView searchText;
    TextView errorText;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        searchText = (TextView) findViewById(R.id.searchText);
        errorText = (TextView) findViewById(R.id.errorText);
        searchProgressBar = (ProgressBar) findViewById(R.id.search_progress_bar);

        KontaktSDK.initialize("nMAQFxJPLLpmxClMDGQZgojcuRJzOZpA");

        proximityManager = new ProximityManager(this);

        checkConnect();

    }

    private void checkConnect() {
        Connect connect = new Connect();

        if (connect.netConnect(this) && connect.bluetoothConnect()) {
            searchProgressBar.setVisibility(View.VISIBLE);
            searchText.setText(getString(R.string.search_beacon));
            proximityManager.setIBeaconListener(createIBeaconListener());
        } else {
            searchProgressBar.setVisibility(View.GONE);
            searchText.setText(getString(R.string.first_error_message));
            errorText.setText(getString(R.string.next_error_message));
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        startScanning();
    }

    @Override
    protected void onStop() {
        searchProgressBar.setVisibility(View.GONE);
        proximityManager.stopScanning();
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        proximityManager.disconnect();
        proximityManager = null;
        super.onDestroy();
    }

    private void startScanning() {
        proximityManager.connect(new OnServiceReadyListener() {
            @Override
            public void onServiceReady() {
                proximityManager.startScanning();
            }
        });
    }

    private IBeaconListener createIBeaconListener() {
        return new SimpleIBeaconListener() {
            @Override
            public void onIBeaconDiscovered(IBeaconDevice ibeacon, IBeaconRegion region) {
                searchProgressBar.setVisibility(View.GONE);
                Log.i("Sample", "IBeacon discovered: " + ibeacon.getUniqueId());
                onStop();

                Intent intent = new Intent(MainActivity.this, InformationActivity.class);
                String uuid = ibeacon.getProximityUUID().toString();
                intent.putExtra("beacon", uuid);
                startActivity(intent);

            }
        };
    }

}