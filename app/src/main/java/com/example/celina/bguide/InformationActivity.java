package com.example.celina.bguide;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.nio.charset.Charset;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class InformationActivity extends AppCompatActivity implements Callback<Question> {

    String beacon;

    RelativeLayout infoLayout;
    TextView title;
    TextView content;
    Retrofit retrofit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_information);

        beacon = getIntent().getStringExtra("beacon");

        infoLayout = (RelativeLayout) findViewById(R.id.info_layout);
        title = (TextView) findViewById(R.id.info_title);
        content = (TextView) findViewById(R.id.info_content);

        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                .create();
        retrofit = new Retrofit.Builder()
                .baseUrl(QuestionAPI.ENDPOINT)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        checkConnect();
    }

    private void checkConnect() {
        Connect connect = new Connect();
        if (connect.netConnect(InformationActivity.this)) {
            QuestionAPI getBeaconAPI = retrofit.create(QuestionAPI.class);
            Call<Question> call = getBeaconAPI.getItems(beacon);
            call.enqueue(this);
        } else {
            String text = getString(R.string.next_error_message);
            Toast.makeText(this, text, Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public void onResponse(Call<Question> call, Response<Question> response) {
        int code = response.code();
        Log.i("code", String.valueOf(code));
        if (code == 200) {
            Question beacon = response.body();
            title.setText(beacon.getTitle());
            content.setText(beacon.getContent());

            Drawable dr = new BitmapDrawable(InformationActivity.this.getResources(), setBitmap(beacon.getPhoto()));
            infoLayout.setBackground(dr);

        } else {
            Toast.makeText(this, "Did not work: " + String.valueOf(code), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onFailure(Call<Question> call, Throwable t) {
        Toast.makeText(this, "Nope", Toast.LENGTH_LONG).show();
    }

    public Bitmap setBitmap(String json_photo) {
        byte[] imageAsBytes = Base64.decode(json_photo.getBytes(Charset.forName("UTF-8")), Base64.DEFAULT);
        Bitmap bp = BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length);

        return bp;
    }
}
