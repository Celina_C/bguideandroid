package com.example.celina.bguide;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;


public interface QuestionAPI {
    String ENDPOINT = "http://192.168.65.11:5000";

    @GET("/api/beacon/{UUID}/information")
    Call<Question> getItems(@Path("UUID") String UUID);
}
