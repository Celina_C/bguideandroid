package com.example.celina.bguide;


public class Question {

    private String id;
    private String title;
    private String content;
    private Photo photo;

    public String getTitle(){
        return title;
    }

    public String getContent(){
        return content;
    }

    public String getPhoto(){
        return photo.getImg();
    }
}

